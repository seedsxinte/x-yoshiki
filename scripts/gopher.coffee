# Description:
#   x-yoshiki bot scripts
# Configuration:
#   None
# Commands:
#   go! go!

module.exports = (robot) ->
  robot.respond /(go|ご|ゴ|疲)/i, (res) ->
    res.reply "ʕ◔ϖ◔ʔ Go! Go!"

