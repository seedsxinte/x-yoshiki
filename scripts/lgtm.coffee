# Description:
#   lgtm from http://www.lgtm.in/
# Configuration:
#   None
# Commands:
#   lgtm - Random LGTM image URL

request = require 'request'
cheerio = require 'cheerio'

module.exports = (robot) ->
  robot.hear /LGTM/i, (res) ->
    url = 'http://www.lgtm.in/g'
    request url, (error, response, body) ->
        if error or response.statusCode != 200
          res.send 'ERROR: 通信に失敗しました'
        else
          $ = cheerio.load body
          res.send $('#imageUrl').val()
