# Description:
#   x-yoshiki bot scripts
# Configuration:
#   None
# Commands:
#   drum - x-yoshikiからひとこと

drumres = [
  'たかが努力じゃないですか',
  '努力すればできるんですよ。じゃあ、努力すればいいじゃないですか',
  'もう本当、これで死んでもいいっていう',
  '何しろ定義づけられちゃうと頭にくる',
  'ガラスのようにパーンっと輝いて，飛び散っちゃえたらいいと思う',
  '生き急いでますよ',
  '密度の濃い時を過ごしたい',
  '頭ごなしに「こういうもの」だっていうのは気に入らない。ぶち壊したくなる',
  '辛(から)い!',
  '俺は本当に気合が入ってるんですよ',
  '安定が嫌い']
module.exports = (robot) ->
  robot.hear /(yoshiki|drum|ドラム|ドラマー|たいこ|太鼓|マーチ)/i, (res) ->
    res.send res.random drumres

