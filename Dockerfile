#FROM glidelabs/alpine:3.3
FROM mhart/alpine-node

RUN apk add --no-cache git
RUN git clone https://bitbucket.org/seedsxinte/x-yoshiki.git
WORKDIR /x-yoshiki

RUN npm install && \
    npm install request cheerio --save


ENTRYPOINT ["/bin/sh" ,"-c", "/x-yoshiki/bin/hubot --adapter slack"]
#ENTRYPOINT ["/bin/sh" ,"-c", "while true; do echo 'hoge'; sleep 3; done"]
